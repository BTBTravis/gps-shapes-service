import aws from 'aws-sdk';

export function sendMessage(str, id) {
    const sqs = new aws.SQS();
    const params = {
        MessageBody: str,
        // QueueUrl: 'https://sqs.us-east-1.amazonaws.com/853019563312/ShapeQueue',
        QueueUrl: `https://sqs.us-east-1.amazonaws.com/${id}/ShapeQueue`,
        DelaySeconds: 0
    };

    return new Promise((resolve, reject) => {
        sqs.sendMessage(params, function(err, data) {
            if (err) {
                console.log(JSON.stringify(err));
                reject(new Error(`failed to send sqs message ${params.MessageBody} to ${params.QueueUrl}`));
            } else {
                resolve('Sent');
            }
        });
    });
}
