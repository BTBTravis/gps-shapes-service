// eslint-disable-next-line import/prefer-default-export
import { sendMessage } from "./sqsPort";

function validateShape(shape) {
    return (
        shape.hasOwnProperty('points')
        && shape.points.hasOwnProperty('length')
        && shape.points.length >= 3
    )
}

function decodeShape(str) {
    return new Promise((resolve, reject) => {
        const shape= typeof str === 'string' ? JSON.parse(str) : str;
        if (!validateShape(shape)) throw new Error("Not a valid shape");
        resolve(shape);
    });
}

export function saveShape(shape, id) {
    return decodeShape(shape)
        .then(decodedShape => sendMessage(JSON.stringify(decodedShape), id));
}
