# GPS Shapes

## Setup Dev Env
To get started you need serverless cli installed and npm.
1. `cd gps-shapes && npm i`
2. `serverless deploy`

After that you can test with curl:
`curl -i -XPOST -H 'Content-Type: application/json' -H 'x-api-key: xxxx' -d '{"points": [1,2,3]}'  'https://xxxxxxx.com/shape'`
        
        
            
