// eslint-disable-next-line import/prefer-default-export
import { saveShape } from "./shapes";

export const create = (event, context, cb) => {
    const id = '853019563312';
    console.log(JSON.stringify(context));
    // context.invokedFunctionArn.split(':')[4];
    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            message: 'Created Shape',
        }),
    };

    saveShape(event.body, id)
        .then((shape) => {
            console.log('---->shape');
            console.log(shape);
        })
        .then(() => cb(null, response))
        .catch(e => cb(null, {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({
                message: e.message
            }),
        }));
};
